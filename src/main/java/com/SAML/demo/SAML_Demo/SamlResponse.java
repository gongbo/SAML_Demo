package com.SAML.demo.SAML_Demo;

import java.util.Objects;

import static org.apache.http.util.TextUtils.isBlank;

public final class SamlResponse {

    private String name;
    private String status;

    private SamlResponse() {
    }

    public static Builder samlResponse() {
        return new Builder();
    }

    public String name() {
        return this.name;
    }

    public String status() {
        return this.status;
    }

    @Override
    public String toString() {
        return "SamlResponse{" +
            "status='" + status + '\'' +
            ", name='" + name + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SamlResponse that = (SamlResponse) o;
        return Objects.equals(name, this.name) &&
            Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, name);
    }

    public boolean isAuthenticationSuccess() {
        return this.status.toLowerCase().equals("success") &&
                !isBlank(this.name);
    }

    public static class Builder {
        private String name;
        private String status;

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public SamlResponse build() {
            SamlResponse samlResponse = new SamlResponse();
            samlResponse.name = this.name;
            samlResponse.status = this.status;
            return samlResponse;
        }
    }
}
