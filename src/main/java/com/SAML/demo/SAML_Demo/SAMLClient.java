package com.SAML.demo.SAML_Demo;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joox.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.joox.JOOX.$;

@Component
public class SAMLClient {
    private static final String SAML_REQUEST_TEMPLATE = "saml-request-template.xml";
    private static final String SAML_REQUEST_URL_TEMPLATE = "%s?SAMLRequest=%s";
    private static final int ISSUE_INSTANT_ADJUST = 1;
    private static final String ISSUE_INSTANT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String BAD_SAML_REQUEST = "";
    private static final String CLAIMS_ACCOUNT_NAME = "Account Name";
    private static final String CLAIMS_EMPLOYEE_NUMBER = "Employee Number";
    private static final String CLAIMS_NAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
    private static final String CLAIMS_GIVENNAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname";
    private static final String CLAIMS_SURNAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname";
    private static final String SAML_RESPONSE_XPATH = "//input[@name='SAMLResponse']";
    private static final int STATUS_SUCCESS = 200;


    private final CloseableHttpClient closeableHttpClient;
//    private final SecurityProperties securityProperties;

    @Autowired
    public SAMLClient(CloseableHttpClient closeableHttpClient) {
        this.closeableHttpClient = closeableHttpClient;
//        this.securityProperties = securityProperties;
    }

    public void sendSAMLRequest() {

    }

    public String metaData() {
        try (InputStream metaDataStream = new ClassPathResource("metadata.xml").getInputStream()) {
            return StreamUtils.copyToString(metaDataStream, UTF_8)
                    .replace("${EntityID}", entityID())
                    .replace("${SingleLogoutServiceLocation}", singleLogoutServiceURL())
                    .replace("${AssertionConsumerServiceLocation}", assertionConsumerServiceURL());
        } catch (Exception e) {
            return null;
        }
    }

    private String extractSamlResponseString(String content) {
        return $(content).xpath(SAML_RESPONSE_XPATH).first().attr("value");
    }

    private boolean successfulResponse(CloseableHttpResponse body) {
        return body.getStatusLine().getStatusCode() == STATUS_SUCCESS;
    }

    private static <T> T attribute(String sampleResponse, String name, Class<T> t) {
        try {
            String text = $(sampleResponse).find("Attribute").matchAttr("Name", name).first().first().text();
            if (Objects.isNull(text)) return null;
            if (Long.class.equals(t)) return t.cast(Long.valueOf(text.trim()));
            if (String.class.equals(t)) return t.cast(text.trim());
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String samlRequestURL() {
        return format(SAML_REQUEST_URL_TEMPLATE,
                idpSSOUrl(), urlEscape(deflateAndBase64Encode(plainSamlRequest(false))));
    }

    public String samlRequestURLLocal() {
        return format(SAML_REQUEST_URL_TEMPLATE,
                idpSSOUrl(), urlEscape(deflateAndBase64Encode(plainSamlRequest(true))));
    }
    private String plainSamlRequest(boolean isLocalUse) {

        try (InputStream samlRequestTemplate = new ClassPathResource(SAML_REQUEST_TEMPLATE).getInputStream()) {
            String request = StreamUtils.copyToString(samlRequestTemplate, UTF_8)
                    .replace("${ID}", requestID())
                    .replace("${IssueInstant}", issueInstant())
                    .replace("${Destination}", idpSSOUrl())
                    .replace("${AssertionConsumerServiceURL}", isLocalUse ? assertionConsumerServiceURLLocal() : assertionConsumerServiceURL())
                    .replace("${Issuer}", issuer());

            return request;
        } catch (Exception e) {
            return BAD_SAML_REQUEST;
        }
    }

    private String issuer() {
        return entityID();
    }

    private String entityID() {
        return "https://uat.otr.mercedes-benz.com.cn/SAML2";
//        return securityProperties.getSaml().getMetadata().getEntityID();
    }

    private String idpSSOUrl() {
        return "https://cdiwl-appstest.i.daimler.com/affwebservices/public/saml2sso";
    }

    private String singleLogoutServiceURL() {
        return "https://uat.otr.mercedes-benz.com.cn/SAML2";
    }

    private String assertionConsumerServiceURL()
    {
        return "https://uat.otr.mercedes-benz.com.cn/SAML2";
    }

    private String assertionConsumerServiceURLLocal() {
        return "http://localhost:8080/login/SAML2/callback";
    }

    private String requestID() {
        // SAML request id can't start with a number or contain spaces, and should have 160 bits of "randomness"
        return format("_%s", UUID.randomUUID());
    }

    private String issueInstant() {
        return DateTime
                .now()
                .minusSeconds(ISSUE_INSTANT_ADJUST)
                .withZone(DateTimeZone.UTC)
                .toString(ISSUE_INSTANT_FORMAT);
    }

    private String urlEscape(String samlRequest) {
        try {
            return URLEncoder.encode(samlRequest, Charset.defaultCharset().name());
        } catch (Exception e) {
            return samlRequest;
        }
    }

    private String deflateAndBase64Encode(String plainSamlRequest) {
        try {
            ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
            Deflater deflater = new Deflater(Deflater.DEFLATED, true);
            DeflaterOutputStream deflaterStream = new DeflaterOutputStream(bytesOut, deflater);
            deflaterStream.write(plainSamlRequest.getBytes(UTF_8));
            deflaterStream.finish();
            return Base64.getEncoder().encodeToString(bytesOut.toByteArray());
        } catch (Exception e) {
            return plainSamlRequest;
        }
    }

    public static String base64Decode(String encodedSamlResponse) {
        try {
            byte[] decodedSamlResponse = Base64.getDecoder().decode(encodedSamlResponse.getBytes(UTF_8));
            return new String(decodedSamlResponse);
        } catch (Exception e) {
            return encodedSamlResponse;
        }
    }

    public SamlResponse resolveResponse(String body) {
        String decode = body; // URLDecoder.decode(body, "UTF-8");

        return Optional.of(body)
                .map(item -> base64Decode(decode.replaceAll("\r\n", "")))
                .map(item -> {
                    Match doc = $(item);
                    return SamlResponse.samlResponse()
                            .withName(getNameId(doc))
                            .withStatus("success")
                            .build();
                }).get();

        //return null;
    }

    public String getNameId(Match xmlDoc) {
        String xpathExpression = "/*[name()='Response']/*[name()='ns2:Assertion']/*[name()='ns2:Subject']/*[name()='ns2:NameID']";
        Match node = xmlDoc.xpath(xpathExpression);

        return node.text();
    }
}
